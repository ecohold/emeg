# EMGE 

app formado por cluster de api distribuidas inmemory (Equilibrio do Mix Energético Global) depende dos requisitos específicos e recursos disponíveis. No entanto, algumas práticas recomendadas gerais incluem:

- Arquitetura orientada a serviços (SOA): 
Uma SOA permite que diferentes componentes do EMGE se comuniquem e interajam de forma flexível e independente.
Use padrões e protocolos abertos: Isso garante interoperabilidade e portabilidade entre diferentes sistemas e componentes.
Implemente segurança robusta: Proteja os dados e sistemas do EMGE contra acesso não autorizado e violações de segurança.
Forneça uma interface de usuário intuitiva: Torne o EMGE fácil de usar para os usuários, independentemente de seu nível técnico.
Integre-se com outros sistemas: Conecte o EMGE a outros sistemas relevantes, como sistemas de gerenciamento de rede, sistemas de faturamento e sistemas de atendimento ao cliente.
Use ferramentas e tecnologias modernas: Aproveite as vantagens das tecnologias mais recentes para melhorar a eficiência, precisão e escalabilidade do EMGE.
Grupo de Classes que Interagem e Alimentam o EMGE

As seguintes classes de sistemas interagem e alimentam o EMGE:

ApurTransmissao: Calcula e gerencia as transmissões de energia entre as usinas e os consumidores.
Carga: Previsão e gerenciamento da demanda de energia pelos consumidores.
DESSEM: Simula e otimiza a operação do sistema elétrico, incluindo geração, transmissão e distribuição.
EnergiaAgora: Fornece dados em tempo real sobre a operação do sistema elétrico.
Hidrologia: Modela e prevê o comportamento hidrológico, como vazão e precipitação.
PDP: Planejamento e programação da geração de energia.
ProcOpRede: Gerencia e otimiza a operação da rede de transmissão.
SGI-OP: Monitora e controla a operação das usinas geradoras.
Teleassistencia: Fornece suporte remoto e monitoramento para usinas geradoras e equipamentos de transmissão.
Essas classes de sistemas trabalham juntas para fornecer ao EMGE as informações e funcionalidades necessárias para gerenciar o sistema elétrico de forma eficiente e confiável.

#-------------------------deepEMEG---------------------------------#

**Descrição dos Arquivos do DESSEM**

**Arquivos de Dados de Entrada**

* **areacont.dat:** Dados de área de controle.
* **cortdeco.rv1:** Cortes gerados pelo DECOMP.
* **cotasr11.dat:** Coeficientes da função de produção das usinas hidrelétricas.
* **curvtviag.dat:** Curvas de variação de vazão dos reservatórios.
* **dadvaz.dat:** Dados de vazões.
* **deflant.dat:** Definições de plantas.
* **des_log_relato.dat:** Log de relato do DESSEM.
* **desselet.dat:** Dados da rede elétrica.
* **dessem.arq:** Arquivo de parâmetros do DESSEM.
* **dessopc.dat:** Dados de otimização.
* **dom15h.pwf:** Arquivo de despacho para o horário de ponta (15h).
* **dom19h.pwf:** Arquivo de despacho para o horário de fora de ponta (19h).
* **entdados.aux:** Dados auxiliares de entrada.
* **entdados.dat:** Dados de entrada.
* **hidr.dat:** Dados das usinas hidrelétricas.
* **ils_tri.dat:** Dados de vazões do Canal Ilha Solteira – Três Irmãos.
* **indice.csv:** Índice dos arquivos de dados.
* **infofcf.dat:** Informações sobre os cortes de Benders.
* **leve.pwf:** Arquivo de despacho para o horário de leveza (0h).
* **mapcut.rv1:** Mapa de cortes.
* **minima.pwf:** Arquivo de despacho para o horário de mínima carga (4h).
* **mlt.dat:** Dados de vazões médias de longo termo.
* **operuh.dat:** Restrições operativas das usinas hidrelétricas.
* **operut.aux:** Dados auxiliares das unidades termoelétricas.
* **operut.dat:** Dados das unidades termoelétricas.
* **pat01.afp:** Arquivo de despacho para o primeiro patamar cronológico.
* **pat02.afp:** Arquivo de despacho para o segundo patamar cronológico.
* **...**
* **pat48.afp:** Arquivo de despacho para o quadragésimo oitavo patamar cronológico.
* **pdo_cmobar.dat:** Dados de custos marginais de operação por barra.
* **pdo_cmosist.dat:** Dados de custos marginais de operação do sistema.
* **pdo_contr.dat:** Dados de contratos.
* **pdo_eolica.dat:** Dados de geração eólica.
* **pdo_hidr.dat:** Dados de geração hidrelétrica.
* **pdo_operacao.dat:** Dados de operação.
* **pdo_oper_contr.dat:** Dados de operação dos contratos.
* **pdo_oper_term.dat:** Dados de operação das unidades termoelétricas.
* **pdo_oper_titulacao_contratos.dat:** Dados de titulação dos contratos.
* **pdo_oper_titulacao_usinas.dat:** Dados de titulação das usinas.
* **pdo_sist.dat:** Dados do sistema.
* **pdo_somflux.dat:** Dados de somatório de fluxos.
* **pdo_sumaoper.dat:** Dados de resumo da operação.
* **pdo_term.dat:** Dados de geração termelétrica.
* **polinjus.csv:** Dados de polinômios de justificativa.
* **ptoper.dat:** Dados de programação da operação.
* **rampas.dat:** Dados de rampas das unidades termoelétricas.
* **renovaveis.dat:** Dados de geração renovável.
* **respot.dat:** Dados de reserva de potência.
* **respotele.dat:** Dados de reserva de potência elétrica.
* **restseg.dat:** Dados de restrições de segurança.
* **rstlpp.dat:** Dados de resultados do LP.
* **termdat.dat:** Dados das usinas termoelétricas.
* **vazaolateral.csv:** Dados de vazões laterais.

**Quem Lê Esses Tipos de Arquivos?**

Os arquivos do DESSEM são lidos por:

* **Operadores do sistema elétrico:** Para planejar e operar o sistema elétrico.
* **Empresas de geração e transmissão:** Para otimizar sua geração e transmissão de energia.
* **Órgãos reguladores:** Para monitorar e regular o setor elétrico.
* **Pesquisadores e acadêmicos:** Para estudar o sistema elétrico e desenvolver novos modelos e algoritmos.

**Arquivos de Entrada do DESSEM**

**Arquivos de Dados**

* **DESSEM.ARQ:** Arquivo índice com informações gerais do caso.
* **DADVAZ.XXX:** Arquivo com dados de vazões naturais.
* **SIMUL.XXX:** Arquivo com dados para o período de simulação.
* **ENTDADOS.XXX:** Arquivo com dados gerais, incluindo configuração do estudo, dados de usinas e demandas.
* **HIDR.DAT:** Arquivo com cadastro das usinas hidrelétricas.
* **TERM.DAT:** Arquivo com cadastro das usinas termoelétricas.
* **OPERUH.XXX:** Arquivo com restrições operativas para usinas hidrelétricas.
* **OPERUT.XXX:** Arquivo com opções de execução e condições operativas para usinas termoelétricas.

**Arquivos da Função de Custo Futuro do DECOMP**

* **MAPCUT.DEC:** Arquivo de mapa para os Cortes de Benders.
* **INFOFCF.DEC:** Arquivo com informações adicionais para os Cortes de Benders.

**Arquivos de Saída do DESSEM**

* **PTOPER.DAT:** Arquivo com o ponto de operação.
* **PDO_SIST.DAT:** Arquivo com dados do sistema, incluindo demandas, geração e intercâmbios.
* **PDO_TERM.DAT:** Arquivo com dados das usinas termoelétricas, incluindo geração e custos.
* **PDO_HIDR.DAT:** Arquivo com dados das usinas hidrelétricas, incluindo geração e vazões.
* **PDO_CONTR.DAT:** Arquivo com dados dos contratos de importação/exportação.
* **PDO_EOLICA.DAT:** Arquivo com dados das fontes renováveis eólicas.
* **PDO_OPER_CONTR.DAT:** Arquivo com dados das operações dos contratos.
* **PDO_OPER_TERM.DAT:** Arquivo com dados das operações das usinas termoelétricas.
* **PDO_OPER_TITULACAO_CONTRATOS.DAT:** Arquivo com dados das titulações dos contratos.
* **PDO_OPER_TITULACAO_USINAS.DAT:** Arquivo com dados das titulações das usinas.
* **PDO_OPERACAO.DAT:** Arquivo com dados da operação, incluindo geração, custos e intercâmbios.
* **PDO_SOMFLUX.DAT:** Arquivo com dados dos fluxos de potência.
* **PDO_SUMAOPER.DAT:** Arquivo com dados resumidos da operação.
* **RESPOT.DAT:** Arquivo com dados das restrições operativas.
* **RESPOTELE.DAT:** Arquivo com dados das restrições operativas elétricas.
* **RESTSEG.DAT:** Arquivo com dados das restrições de segurança.
* **RSTLPP.DAT:** Arquivo com dados das restrições de perdas nas linhas de transmissão.

**Organização dos Arquivos**

Os arquivos podem ser organizados por tipo, função ou API:

**Tipo**

* Arquivos de dados
* Arquivos da função de custo futuro do DECOMP
* Arquivos de saída do DESSEM

**Função**

* Dados de entrada
* Dados de saída
* Restrições operativas
* Cadastro de usinas
* Opções de execução

**API**

* Arquivos lidos pelo DESSEM
* Arquivos gerados pelo DESSEM

**Leitura, Análise e Interpretação de Dados**

Os dados podem ser lidos usando bibliotecas de leitura de arquivos, como Pandas ou NumPy. Após a leitura, os dados podem ser analisados e interpretados para gerar insights sobre a operação do sistema elétrico.

**Treinamento do DeepModel**

Os dados interpretados podem ser usados para treinar um DeepModel que pode gerar a próxima saída do DESSEM. O DeepModel pode ser treinado usando técnicas de aprendizado de máquina, como redes neurais ou árvores de decisão.


# A estrutura do front-end é baseada em ReactJS, mas pode ser adaptada para outras frameworks.

Back-End:

├── backend/

│  ├── api/

│  │  ├── controllers/

│  │  │  ├── DespachoController.py (Singleton Pattern, Command Pattern)

│  │  │  ├── DadosController.py (Facade Pattern, Data Access Object Pattern)

│  │  │  └── UsuariosController.py (Authentication Pattern, Authorization Pattern)

│  │  ├── models/

│  │  │  ├── Despacho.py (Builder Pattern, Prototype Pattern)

│  │  │  ├── Geracao.py (Value Object Pattern)

│  │  │  ├── Rede.py (Flyweight Pattern, Memento Pattern)

│  │  │  └── Usuario.py (Composite Pattern, Visitor Pattern)

│  │  └── routes.py (Front Controller Pattern)

│  ├── despacho/

│  │  ├── ProcessamentoDados.py (Chain of Responsibility Pattern, Template Method Pattern)

│  │  └── Algoritmos.py (Strategy Pattern, Observer Pattern)

│  ├── data/

│  │  ├── entrada/

│  │  │  ├── historico_operacao.csv

│  │  │  ├── rede.json

│  │  │  ├── geracao.csv

│  │  │  └── carga.csv

│  │  └── saida/

│  │    └── resultados_despacho.csv

│  ├── tests/

│  │  ├── unit/

│  │  │  ├── test_despacho.py

│  │  │  └── test_dados.py

│  │  ├── integration/

│  │  │  └── test_api.py

│  │  └── functional/

│  │    └── test_despacho_completo.py

│  ├── profiling/

│  │  └── profile_despacho.py

│  └── stress/

│    └── stress_test_api.py

└── frontend/

    ├── public/

    │  └── (arquivos públicos do front end)

    └── src/

        ├── app/

        │  ├── App.js (Singleton Pattern)

        │  ├── components/

        │  │  ├── Despacho.js (Observer Pattern, State Pattern)

        │  │  ├── Geracao.js (Presenter Pattern)

        │  │  └── Rede.js (Model-View-Controller Pattern)

        │  ├── pages/

        │  │  ├── Home.js

        │  │  └── DespachoPage.js (Mediator Pattern)

        │  └── styles/

        │    └── styles.css

        ├── node_modules/

        ├── package.json

        ├── components/

        │  ├── TreeMap/

        │  │  └── (componente TreeMap)

        │  ├── GraphVisualization/

        │  │  └── (componente de visualização por grafos)

        │  └── GeoLocation/

        │    └── (componente de geolocalização)

        └── tests/

        ├── unit/

        │  └── (testes de unidade)

        ├── integration/

        │  └── (testes de integração)

        ├── functional/

        │  └── (testes funcionais)

        ├── profiling/

        │  └── (testes de profiling)

        └── stress/

            └── (testes de estresse)



1. **backend/**: Esta pasta contém todo o código relacionado ao back-end do projeto.

  - **api/**: Esta pasta contém os controladores e rotas da API.

   - **controllers/**: Contém os controladores responsáveis por lidar com as requisições HTTP.

    - **DespachoController.py**: Controlador para operações relacionadas ao despacho.

    - **DadosController.py**: Controlador para operações relacionadas aos dados.

    - **UsuariosController.py**: Controlador para operações relacionadas aos usuários.

   - **models/**: Contém os modelos de dados da aplicação.

    - **Despacho.py**: Modelo para despacho de energia.

    - **Geracao.py**: Modelo para dados de geração de energia.

    - **Rede.py**: Modelo para rede de distribuição de energia.

    - **Usuario.py**: Modelo para usuários do sistema.

   - **routes.py**: Arquivo de configuração de rotas da API.



  - **despacho/**: Contém módulos relacionados ao despacho de energia.

   - **ProcessamentoDados.py**: Módulo para processamento de dados relacionados ao despacho.

   - **Algoritmos.py**: Módulo com algoritmos relacionados ao despacho de energia.



  - **data/**: Contém os dados de entrada e saída do sistema.

   - **entrada/**: Pasta com os arquivos de entrada.

    - **historico_operacao.csv**: Arquivo de histórico de operações.

    - **rede.json**: Arquivo JSON com dados da rede elétrica.

    - **geracao.csv**: Arquivo CSV com dados de geração de energia.

    - **carga.csv**: Arquivo CSV com dados de carga de energia.

   - **saida/**: Pasta para armazenar os resultados do despacho.



  - **tests/**: Contém os testes automatizados do sistema.

   - **unit/**: Testes unitários.

    - **test_despacho.py**: Testes para o módulo de despacho.

    - **test_dados.py**: Testes para o módulo de dados.

   - **integration/**: Testes de integração.

    - **test_api.py**: Testes para a API.

   - **functional/**: Testes funcionais.

    - **test_despacho_completo.py**: Testes completos para o despacho de energia.



  - **profiling/**: Contém scripts para análise de desempenho do despacho de energia.

   - **profile_despacho.py**: Script para perfilamento do despacho.



  - **stress/**: Contém scripts para testes de estresse da API.

   - **stress_test_api.py**: Script para teste de estresse da API.



2. **frontend/**: Esta pasta contém todo o código relacionado ao front-end do projeto.

  - **public/**: Contém os arquivos públicos do front-end, como HTML e imagens.

  - **src/**: Contém o código-fonte do front-end.

   - **app/**: Contém o aplicativo React principal.

    - **App.js**: Componente principal do aplicativo.

    - **components/**: Contém os componentes React reutilizáveis.

     - **Despacho.js**: Componente para exibição e controle do despacho de energia.

     - **Geracao.js**: Componente para exibição de dados de geração de energia.

     - **Rede.js**: Componente para visualização da rede elétrica.

    - **pages/**: Contém os componentes de página do aplicativo.

     - **Home.js**: Página inicial do aplicativo.

     - **DespachoPage.js**: Página de despacho de energia.

    - **styles/**: Contém os arquivos de estilo CSS do aplicativo.



   - **node_modules/**: Contém as dependências do Node.js.



   - **package.json**: Arquivo de configuração do Node.js.



  - **components/**: Contém componentes adicionais para o front-end.

   - **TreeMap/**: Componente para visualização de treemap.

   - **GraphVisualization/**: Componente para visualização de grafos.

   - **GeoLocation/**: Componente para geolocalização.


  - **tests/**: Contém os testes automatizados do front-end.

   - **unit/**: Testes unitários do front-end.

   - **integration/**: Testes de integração do front-end.

   - **functional/**: Testes funcionais do front-end.

   - **profiling/**: Testes de perfilamento do front-end.

   - **stress/**: Testes de estresse do front-end.

   #--------------------------sob o nivel -------------------------------#

   requisitos e logica de negocio upgrade DeepSolar padrão senior development consultor gpt gemini gcp



usuario ou bot navegando na internet via browser desktop ou mobile pelo gmap gearth e o app 

1) identifica imagens estaticas ou video dinamico (object detect image classificastion and mask) areas com paineis fotovoltaicos em residencias galpoes etc ou em usinas solares e calcula em tempo real o potencial solar total e o valor naquele momento em função da intensidade solar e as receitas nominais e em tempo real capex opex roi, ponto de equilibrio

2) identifica telhados residenciais galpoes etc, areas livres deserticas e potencial solar por km2, capex opex roi, ponto de equilibrio



3) implementado no DeepSolar_timelapse coloque um * (novas funcionalidade) 



DeepSolar_timelapse/

├── bass_model_fit_adoption_curves.py     # Ajuste de curvas de adoção do modelo Bass.

├── bass_model_parameter_phase_analysis.ipynb # Análise das fases dos parâmetros do modelo Bass.

├── bass_model_parameter_regression.ipynb   # Análise de regressão dos parâmetros do modelo Bass.

├── generate_anchor_image_dict.ipynb      # Geração de dicionário de imagens âncora.

├── hp_search_HR.py              # Busca de hiperparâmetros para o modelo HR.

├── hp_search_LR_rgb.py            # Busca de hiperparâmetros para o modelo LR.

├── hp_search_ood_multilabels.py        # Busca de hiperparâmetros para o modelo OOD.

├── __init__.py                # Inicialização do pacote.

├── LICENSE.md                 # Arquivo de licença do projeto.

├── LR_models/

│  ├── __init__.py              # Inicialização do pacote.

│  ├── siamese_model_binary_layerwise.py   # Modelo siamês binário com camadas.

│  ├── siamese_model_binary.py        # Modelo siamês binário.

│  └── siamese_model_rgb.py          # Modelo siamês RGB.

├── predict_HR.py               # Predição utilizando o modelo HR.

├── predict_installation_year_from_image_sequences.ipynb # Previsão do ano de instalação a partir de sequências de imagens.

├── predict_LR_rgb.py             # Predição utilizando o modelo LR.

├── predict_ood_multilabels.py         # Predição utilizando o modelo OOD.

├── README.md                 # README do projeto.

├── requirements.txt              # Dependências do projeto.

├── solar_potential_detection.py        # * Detecção de potencial solar em imagens e vídeos.

├── rooftop_detection.py            # * Detecção de telhados em imagens e vídeos.

└── utils/

  ├── image_dataset.py            # Manipulação de conjuntos de dados de imagens.

  ├── inception_modified.py         # Modificações na arquitetura Inception.

  ├── solar_potential_calculator.py     # * Cálculo do potencial solar total e valor em tempo real.

  └── __init__.py              # Inicialização do pacote.





#------------------------------e integre ao deepEMEG-----------------------#



deepEMGE/

├── backend/                 # Pasta principal do backend.

│  ├── api/                 # Pasta contendo a lógica da API.

│  │  ├── controllers/           # Pasta contendo os controladores da API.

│  │  │  ├── DespachoController.py    # Controlador para operações relacionadas ao despacho de energia.

│  │  │  ├── DadosController.py      # Controlador para operações relacionadas aos dados do sistema elétrico.

│  │  │  └── UsuariosController.py    # Controlador para operações relacionadas aos usuários do sistema.

│  │  └── models/             # Pasta contendo os modelos da API.

│  │    ├── Despacho.py         # Modelo para representar o despacho de energia.

│  │    ├── Geracao.py          # Modelo para representar os dados de geração de energia.

│  │    ├── Rede.py           # Modelo para representar a rede elétrica.

│  │    └── Usuario.py          # Modelo para representar os usuários do sistema.

│  │  └── routes.py            # Arquivo contendo as rotas da API.

│  ├── despacho/              # Pasta contendo lógica específica de despacho de energia.

│  │  ├── ProcessamentoDados.py      # Módulo para processamento de dados relacionados ao despacho de energia.

│  │  └── Algoritmos.py          # Módulo com algoritmos relacionados ao despacho de energia.

│  ├── data/                # Pasta contendo dados do sistema elétrico.

│  │  ├── entrada/             # Pasta contendo dados de entrada.

│  │  │  ├── historico_operacao.csv    # Arquivo contendo dados históricos de operação do sistema elétrico.

│  │  │  ├── rede.json          # Arquivo contendo dados da rede elétrica.

│  │  │  ├── geracao.csv         # Arquivo contendo dados de geração de energia.

│  │  │  ├── carga.csv          # Arquivo contendo dados de carga de energia.

│  │  └── saida/              # Pasta contendo dados de saída.

│  │    └── resultados_despacho.csv   # Arquivo para armazenar os resultados do despacho de energia.

│  ├── tests/                # Pasta contendo testes.

│  │  ├── unit/              # Pasta contendo testes unitários.

│  │  │  ├── test_despacho.py       # Testes unitários para o módulo de despacho de energia.

│  │  │  ├── test_dados.py        # Testes unitários para o módulo de dados do sistema elétrico.

│  │  ├── integration/           # Pasta contendo testes de integração.

│  │  │  └── test_api.py         # Testes de integração para a API do sistema.

│  │  └── functional/           # Pasta contendo testes funcionais.

│  │    └── test_despacho_completo.py  # Testes funcionais completos para o despacho de energia.

│  ├── profiling/              # Pasta contendo testes de profiling.

│  │  └── profile_despacho.py       # Teste de profiling para avaliar o desempenho do despacho de energia.

│  └── stress/               # Pasta contendo testes de estresse.

│    └── stress_test_api.py        # Testes de estresse para avaliar o desempenho da API do sistema.

└── frontend/                # Pasta principal do frontend.

  ├── public/               # Pasta contendo arquivos públicos do frontend.

  │  └── (arquivos públicos do front end)

  └── src/                 # Pasta contendo código-fonte do frontend.

    ├── app/               # Pasta contendo a aplicação principal do frontend.

    │  ├── App.js            # Arquivo principal da aplicação.

    │  ├── components/         # Pasta contendo componentes do frontend.

    │  │  ├── Despacho.js       # Componente para exibir informações de despacho de energia.

    │  │  ├── Geracao.js        # Componente para exibir informações de geração de energia.

    │  │  └── Rede.js         # Componente para exibir informações da rede elétrica.

    │  ├── pages/            # Pasta contendo páginas do frontend.

    │  │  ├── Home.js         # Página inicial da aplicação.

    │  │  └── DespachoPage.js     # Página para exibir informações de despacho de energia.

    │  └── styles/           # Pasta contendo estilos do frontend.

    │    └── styles.css        # Arquivo de estilos CSS.

    ├── node_modules/          # Pasta contendo os módulos do Node.js.

    ├── package.json           # Arquivo de manifesto do Node.js.

    ├── components/           # Pasta contendo componentes reutilizáveis do frontend.

    │  ├── TreeMap/          # Componente de TreeMap.

    │  ├── GraphVisualization/     # Componente de visualização por grafos.

    │  └── GeoLocation/        # Componente de geolocalização.

    └── tests/              # Pasta contendo testes do frontend.

      ├── unit/            # Pasta contendo testes de unidade.

      │  └── test_frontend_unit.py # Testes de unidade do front-end.

      ├── integration/        # Pasta contendo testes de integração.

      │  └── test_frontend_integration.py # Testes de integração do front-end.

      ├── functional/         # Pasta contendo testes funcionais.

      │  └── test_frontend_functional.py # Testes funcionais do front-end.

      ├── profiling/         # Pasta contendo testes de profiling.

      │  └── test_frontend_profiling.py # Testes de profiling do front-end.

      └── stress/           # Pasta contendo testes de estresse.

        └── test_frontend_stress.py # Testes de estresse do front-end.







#---------------------integração DeepSolar++ e deepEMEG---------------------#

deepEMGE/
├── backend/                          # Pasta principal do backend.
│   ├── api/                          # Pasta contendo a lógica da API.
│   │   ├── controllers/             # Pasta contendo os controladores da API.
│   │   │   ├── DespachoController.py    # Controlador para operações relacionadas ao despacho de energia.
│   │   │   ├── DadosController.py      # Controlador para operações relacionadas aos dados do sistema elétrico.
│   │   │   └── UsuariosController.py    # Controlador para operações relacionadas aos usuários do sistema.
│   │   └── models/                   # Pasta contendo os modelos da API.
│   │       ├── Despacho.py              # Modelo para representar o despacho de energia.
│   │       ├── Geracao.py               # Modelo para representar os dados de geração de energia.
│   │       ├── Rede.py                  # Modelo para representar a rede elétrica.
│   │       └── Usuario.py               # Modelo para representar os usuários do sistema.
│   │   └── routes.py                 # Arquivo contendo as rotas da API.
│   ├── despacho/                     # Pasta contendo lógica específica de despacho de energia.
│   │   ├── ProcessamentoDados.py         # Módulo para processamento de dados relacionados ao despacho de energia.
│   │   └── Algoritmos.py             # Módulo com algoritmos relacionados ao despacho de energia.
│   ├── data/                         # Pasta contendo dados do sistema elétrico.
│   │   ├── entrada/                      # Pasta contendo dados de entrada.
│   │   │   ├── historico_operacao.csv      # Arquivo contendo dados históricos de operação do sistema elétrico.
│   │   │   ├── rede.json                   # Arquivo contendo dados da rede elétrica.
│   │   │   ├── geracao.csv                 # Arquivo contendo dados de geração de energia.
│   │   │   ├── carga.csv                   # Arquivo contendo dados de carga de energia.
│   │   └── saida/                       # Pasta contendo dados de saída.
│   │       └── resultados_despacho.csv    # Arquivo para armazenar os resultados do despacho de energia.
│   ├── tests/                        # Pasta contendo testes.
│   │   ├── unit/                      # Pasta contendo testes unitários.
│   │   │   ├── test_despacho.py            # Testes unitários para o módulo de despacho de energia.
│   │   │   └── test_dados.py             # Testes unitários para o módulo de dados do sistema elétrico.
│   │   ├── integration/                # Pasta contendo testes de integração.
│   │   │   └── test_api.py              # Testes de integração para a API do sistema.
│   │   └── functional/                 # Pasta contendo testes funcionais.
│   │       └── test_despacho_completo.py    # Testes funcionais completos para o despacho de energia.
│   ├── profiling/                    # Pasta contendo testes de profiling.
│   │   └── profile_despacho.py          # Teste de profiling para avaliar o desempenho do despacho de energia.
│   └── stress/                       # Pasta contendo testes de estresse.
│       └── stress_test_api.py           # Testes de estresse para avaliar o desempenho da API do sistema.
├── frontend/                         # Pasta principal do frontend.
│   ├── public/                      # Pasta contendo arquivos públicos do frontend.
│   │   └── (arquivos públicos do front end)
│   └── src/                        # Pasta contendo código-fonte do frontend.
│       ├── app/                    # Pasta contendo a aplicação principal do frontend.
│       │   ├── App.js                 # Arquivo principal da aplicação.
│       │   ├── components/            # Pasta contendo componentes do frontend.
│       │   │   ├── Despacho.js          # Componente para exibir informações de despacho de energia.
│       │   │   ├── Geracao.js           # Componente para exibir informações de geração de energia.
│       │   │   └── Rede.js            # Componente para exibir informações da rede elétrica.
│       │   ├── pages/                 # Pasta contendo páginas do frontend.
│       │   │   ├── Home.js              # Página inicial da aplicação.
│       │   │   └── DespachoPage.js      # Página para exibir informações de despacho de energia.
│       │   └── styles/                # Pasta contendo estilos do frontend.
│       │       └── styles.css           # Arquivo de estilos CSS.
│       ├── node_modules/               # Pasta contendo os módulos do Node.js.
│       ├── package.json                # Arquivo de manifesto do Node.js.
│       ├── components/                 # Pasta contendo componentes reutilizáveis do frontend.
│       │   ├── TreeMap/               # Componente de TreeMap.
│       │   ├── GraphVisualization/    # Componente de visualização por grafos.
│       │   └── GeoLocation/           # Componente de geolocalização.
│       └── tests/                     # Pasta contendo testes do frontend.
│           ├── unit/                   # Pasta contendo testes de unidade.
│           │   └── test_frontend_unit.py  # Testes de unidade do front-end.
│           ├── integration/             # Pasta contendo testes de integração.
│           │   └── test_frontend_integration.py # Testes de integração do front-end.
│           ├── functional/              # Pasta contendo testes funcionais.
│           │   └── test_frontend_functional.py # Testes funcionais do front-end.
│           ├── profiling/               # Pasta contendo testes de profiling.
│           │   └── test_frontend_profiling.py # Testes de profiling do front-end.
│           └── stress/                  # Pasta contendo testes de estresse.
│               └── test_frontend_stress.py # Testes de estresse do front-end.
├── DeepSolar_timelapse/                   # Pasta contendo o projeto DeepSolar_timelapse.
│   ├── bass_model_fit_adoption_curves.py     # Ajuste de curvas de adoção do modelo Bass.
│   ├── bass_model_parameter_phase_analysis.ipynb # Análise das fases dos parâmetros do modelo Bass.
│   ├── bass_model_parameter_regression.ipynb   # Análise de regressão dos parâmetros do modelo Bass.
│   ├── generate_anchor_image_dict.ipynb      # Geração de dicionário de imagens âncora.
│   ├── hp_search_HR.py              # Busca de hiperparâmetros para o modelo HR.
│   ├── hp_search_LR_rgb.py            # Busca de hiperparâmetros para o modelo LR.
│   ├── hp_search_ood_multilabels.py        # Busca de hiperparâmetros para o modelo OOD.
│   ├── __init__.py                # Inicialização do pacote.
│   ├── LICENSE.md                 # Arquivo de licença do projeto.
│   ├── LR_models/                  # Pasta contendo modelos LR.
│   │   ├── __init__.py              # Inicialização do pacote.
│   │   ├── siamese_model_binary_layerwise.py   # Modelo siamês binário com camadas.
│   │   ├── siamese_model_binary.py        # Modelo siamês binário.
│   │   └── siamese_model_rgb.py          # Modelo siamês RGB.
│   ├── predict_HR.py               # Predição utilizando o modelo HR.
│   ├── predict_installation_year_from_image_sequences.ipynb # Previsão do ano de instalação a partir de sequências de imagens.
│   ├── predict_LR_rgb.py             # Predição utilizando o modelo LR.
│   ├── predict_ood_multilabels.py         # Predição utilizando o modelo OOD.
│   ├── README.md                 # README do projeto.
│   ├── requirements.txt              # Dependências do projeto.
│   ├── solar_potential_detection.py        # Detecção de potencial solar em imagens e vídeos.
│   ├── rooftop_detection.py            # Detecção de telhados em imagens e vídeos.
│   └── utils/                     # Pasta contendo utilitários.
│       ├── image_dataset.py            # Manipulação de conjuntos de dados de imagens.
│       ├── inception_modified.py         # Modificações na arquitetura Inception.
│       └── solar_potential_calculator.py     # Cálculo do potencial solar total e valor em tempo real.

#---------------------------arquitetura e codigo -------------------------------#



deepEMGE/
├── backend/                          # Pasta principal do backend.
│   ├── api/                          # Pasta contendo a lógica da API.
│   │   ├── controllers/             # Pasta contendo os controladores da API.
│   │   │   ├── DespachoController.py    # Controlador para operações relacionadas ao despacho de energia.
│   │   │   ├── DadosController.py      # Controlador para operações relacionadas aos dados do sistema elétrico.
│   │   │   └── UsuariosController.py    # Controlador para operações relacionadas aos usuários do sistema.
│   │   └── models/                   # Pasta contendo os modelos da API.
│   │       ├── Despacho.py              # Modelo para representar o despacho de energia.
│   │       ├── Geracao.py               # Modelo para representar os dados de geração de energia.
│   │       ├── Rede.py                  # Modelo para representar a rede elétrica.
│   │       └── Usuario.py               # Modelo para representar os usuários do sistema.
│   │   └── routes.py                 # Arquivo contendo as rotas da API.
│   ├── despacho/                     # Pasta contendo lógica específica de despacho de energia.
│   │   ├── ProcessamentoDados.py         # Módulo para processamento de dados relacionados ao despacho de energia.
│   │   └── Algoritmos.py             # Módulo com algoritmos relacionados ao despacho de energia.
│   ├── data/                         # Pasta contendo dados do sistema elétrico.
│   │   ├── entrada/                      # Pasta contendo dados de entrada.
│   │   │   ├── historico_operacao.csv      # Arquivo contendo dados históricos de operação do sistema elétrico.
│   │   │   ├── rede.json                   # Arquivo contendo dados da rede elétrica.
│   │   │   ├── geracao.csv                 # Arquivo contendo dados de geração de energia.
│   │   │   ├── carga.csv                   # Arquivo contendo dados de carga de energia.
│   │   └── saida/                       # Pasta contendo dados de saída.
│   │       └── resultados_despacho.csv    # Arquivo para armazenar os resultados do despacho de energia.
│   ├── tests/                        # Pasta contendo testes.
│   │   ├── unit/                      # Pasta contendo testes unitários.
│   │   │   ├── test_despacho.py            # Testes unitários para o módulo de despacho de energia.
│   │   │   └── test_dados.py             # Testes unitários para o módulo de dados do sistema elétrico.
│   │   ├── integration/                # Pasta contendo testes de integração.
│   │   │   └── test_api.py              # Testes de integração para a API do sistema.
│   │   └── functional/                 # Pasta contendo testes funcionais.
│   │       └── test_despacho_completo.py    # Testes funcionais completos para o despacho de energia.
│   ├── profiling/                    # Pasta contendo testes de profiling.
│   │   └── profile_despacho.py          # Teste de profiling para avaliar o desempenho do despacho de energia.
│   └── stress/                       # Pasta contendo testes de estresse.
│       └── stress_test_api.py           # Testes de estresse para avaliar o desempenho da API do sistema.
├── frontend/                         # Pasta principal do frontend.
│   ├── public/                      # Pasta contendo arquivos públicos do frontend.
│   │   └── (arquivos públicos do front end)
│   └── src/                        # Pasta contendo código-fonte do frontend.
│       ├── app/                    # Pasta contendo a aplicação principal do frontend.
│       │   ├── App.js                 # Arquivo principal da aplicação.
│       │   ├── components/            # Pasta contendo componentes do frontend.
│       │   │   ├── Despacho.js          # Componente para exibir informações de despacho de energia.
│       │   │   ├── Geracao.js           # Componente para exibir informações de geração de energia.
│       │   │   └── Rede.js            # Componente para exibir informações da rede elétrica.
│       │   ├── pages/                 # Pasta contendo páginas do frontend.
│       │   │   ├── Home.js              # Página inicial da aplicação.
│       │   │   └── DespachoPage.js      # Página para exibir informações de despacho de energia.
│       │   └── styles/                # Pasta contendo estilos do frontend.
│       │       └── styles.css           # Arquivo de estilos CSS.
│       ├── node_modules/               # Pasta contendo os módulos do Node.js.
│       ├── package.json                # Arquivo de manifesto do Node.js.
│       ├── components/                 # Pasta contendo componentes reutilizáveis do frontend.
│       │   ├── TreeMap/               # Componente de TreeMap.
│       │   ├── GraphVisualization/    # Componente de visualização por grafos.
│       │   └── GeoLocation/           # Componente de geolocalização.
│       └── tests/                     # Pasta contendo testes do frontend.
│           ├── unit/                   # Pasta contendo testes de unidade.
│           │   └── test_frontend_unit.py  # Testes de unidade do front-end.
│           ├── integration/             # Pasta contendo testes de integração.
│           │   └── test_frontend_integration.py # Testes de integração do front-end.
│           ├── functional/              # Pasta contendo testes funcionais.
│           │   └── test_frontend_functional.py # Testes funcionais do front-end.
│           ├── profiling/               # Pasta contendo testes de profiling.
│           │   └── test_frontend_profiling.py # Testes de profiling do front-end.
│           └── stress/                  # Pasta contendo testes de estresse.
│               └── test_frontend_stress.py # Testes de estresse do front-end.

#   DeepSolar_timelapse/                   # Pasta contendo o projeto DeepSolar_timelapse.
#   ├── # bass_model_fit_adoption_curves.py     # Ajuste de curvas de adoção do modelo Bass.
[import numpy as np
import pandas as pd
import random
import os
from os.path import join, exists
import pickle
from tqdm import tqdm
import matplotlib.pyplot as plt
from matplotlib import patches
from copy import deepcopy
from utils import *
import statsmodels.api as sm
import statsmodels.formula.api as smf
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score
from scipy import stats
from scipy.optimize import least_squares

"""
This script is for fitting the Bass model curve for the solar adoption curve in each census block
group. For each block group, there will be a set of estimated Bass model parameters, include d, p,
q, m.
"""
price_trend = np.array([10.0, 10.1, 9.6, 9.2, 7.9, 7.0, 5.9, 5.1, 4.8, 4.7, 4.4])
log_price_trend = np.log(price_trend / price_trend[0])

def get_cum_installations(coefs, t):
    """Given Bass model coefficients, and a time array, return the array of cumulative values."""
    p, q, m, d = coefs
    y = (1 - np.exp(-(p + q) * (t - d))) / (1 + q / p * np.exp(-(p + q) * (t - d)))
    return m * y * (t >= d) + 0 * (t < d)

def get_residuals_cum(coefs, t, y_true):
    """
    Given Bass model coefficients, and a time array, return resididual between true cumulative
    values and predicted cumulative values (predicted by Bass model).
    """
    y_pred = get_cum_installations(coefs, t)
    return y_pred - y_true

def get_cum_installations_GBM(coefs, t):
    """
    Given Generalized Bass model coefficients, and a time array, return the array of 
    cumulative values.
    """
    p, q, m, d, beta = coefs
    y = (1 - np.exp(-(p + q) * (t - d + beta * log_price_trend))) / (1 + q / p * np.exp(-(p + q) * (t - d + beta * log_price_trend)))
    return m * y * (t >= d) + 0 * (t < d)

def get_residuals_cum_GBM(coefs, t, y_true):
    """
    Given Generalized Bass model coefficients, and a time array, return resididual between true
    cumulative values and predicted cumulative values (predicted by Bass model).
    """
    y_pred = get_cum_installations_GBM(coefs, t)
    return y_pred - y_true

def get_best_fit_NLS(y_arr, dy_arr, initial_p=0.01, initial_q=0.5, initial_m=None, upper_m=np.inf):
    """
    Given the arrays of cumulative installations in previous year and new installations in current year,
    fit the Bass model curve using Non-Linear Least Square (NLS) with initial values and bounds.
    Different initial values of d (onset of adoption) will be tried to get its best initial value.
    Args:
        y_arr: the array of cumulative installations in previous year.
        dy_arr: the array of new installations in present year.
        initial_p: initial value of p.
        initial_q: initial value of q.
        initial_m: initial value of m.
        upper_m: the upper bound of m.
    Return:
        min_rmse: the minimum RMSE of fitting.
        best_params: the best set of Bass model parameters (p, q, m, d).
        best_y_pred: the prediction of cumulative installations under the best parameters.
        best_dy_pred: the prediction of yearly new installations under the best parameters.
        best_onset_idx: the best initial value of d that will yield the minimum RMSE.
    """
    y_true = np.array(list(y_arr[1:]) + [y_arr[-1] + dy_arr[-1]])
    if initial_m is None:
        initial_m = y_true[-1]
    min_rmse = np.inf
    best_params = None
    best_y_pred = None
    best_onset_idx = None
    for onset_idx in range(-5, 11):
        ls_model = least_squares(
            get_residuals_cum,
            x0=np.array([initial_p, initial_q, initial_m, onset_idx]), 
            jac='cs',
            bounds=([0, 0, 0, -8], [1, np.inf, upper_m, np.inf]),
            args=(np.arange(0, 11), y_true),
            method='trf',
        )
        y_pred = get_cum_installations(ls_model.x, np.arange(0, 11))
        rmse = np.sqrt(np.sum((y_pred - y_true) ** 2))
        p, q, m, d = ls_model.x
        if rmse < min_rmse and p > 0 and q > 0 and m > 0:
            min_rmse = rmse
            best_params = ls_model.x
            best_y_pred = y_pred
            best_onset_idx = onset_idx
    best_dy_pred = np.concatenate([[best_y_pred[0]], best_y_pred[1:] - best_y_pred[:-1]])
    return min_rmse, best_params, best_y_pred, best_dy_pred, best_onset_idx

def get_best_fit_NLS_GBM(y_arr, dy_arr, initial_p=0.01, initial_q=0.5, initial_m=None, initial_beta=-0.4):
    """
    Given the arrays of cumulative installations in previous year and new installations in current year,
    fit the Generalized Bass model (GBM) curve using Non-Linear Least Square (NLS) with initial values and bounds.
    Different initial values of d (onset of adoption) will be tried to get its best initial value.
    Args:
        y_arr: the array of cumulative installations in previous year.
        dy_arr: the array of new installations in present year.
        initial_p: initial value of p.
        initial_q: initial value of q.
        initial_m: initial value of m.
        upper_m: the upper bound of m.
        unitial_beta: the initial value of beta.
    Return:
        min_rmse: the minimum RMSE of fitting.
        best_params: the best set of Bass model parameters (p, q, m, d, beta).
        best_y_pred: the prediction of cumulative installations under the best parameters.
        best_dy_pred: the prediction of yearly new installations under the best parameters.
        best_onset_idx: the best initial value of d that will yield the minimum RMSE.
    """
    y_true = np.array(list(y_arr[1:]) + [y_arr[-1] + dy_arr[-1]])
    if initial_m is None:
        initial_m = y_true[-1]
    min_rmse = np.inf
    best_params = None
    best_y_pred = None
    best_onset_idx = None
    for onset_idx in range(-5, 11):
        ls_model = least_squares(
            get_residuals_cum_GBM,
            x0=np.array([initial_p, initial_q, initial_m, onset_idx, initial_beta]), 
            jac='cs',
            bounds=([0, 0, 0, -np.inf, -np.inf], [1, np.inf, np.inf, np.inf, 0]),
            args=(np.arange(0, 11), y_true),
            method='trf',
        )
        y_pred = get_cum_installations_GBM(ls_model.x, np.arange(0, 11))
        rmse = np.sqrt(np.sum((y_pred - y_true) ** 2))
        p, q, m, d, beta = ls_model.x
        if rmse < min_rmse and p > 0 and q > 0 and m > 0:
            min_rmse = rmse
            best_params = ls_model.x
            best_y_pred = y_pred
            best_onset_idx = onset_idx
    best_dy_pred = np.concatenate([[best_y_pred[0]], best_y_pred[1:] - best_y_pred[:-1]])
    return min_rmse, best_params, best_y_pred, best_dy_pred, best_onset_idx

# 1. Load and process data
bg = pd.read_csv('results/merged_bg.csv')

bg = bg[['blockgroup_FIPS', 'year', 'num_of_installations', 'num_of_buildings_lt600']]

bg['tract_FIPS'] = bg['blockgroup_FIPS'] // 10
bg['county_FIPS'] = bg['blockgroup_FIPS'] // 10000000
bg['state_FIPS'] = bg['blockgroup_FIPS'] // 10000000000

df = bg.sort_values(['year', 'blockgroup_FIPS'])

cumulative_pv_count_dict = {}
cumulative_pv_count = np.array(df[df['year'] == 2005]['num_of_installations'])
cumulative_pv_count_dict[2005] = cumulative_pv_count.copy()
for year in range(2006, 2018):
    cumulative_pv_count = cumulative_pv_count + np.array(df[df['year'] == year]['num_of_installations'])
    cumulative_pv_count_dict[year] = cumulative_pv_count.copy()
    
df['cum_num_of_installations'] = np.concatenate([cumulative_pv_count_dict[x] for x in cumulative_pv_count_dict])
df.sort_values(['blockgroup_FIPS', 'year'], inplace=True)

prev_year_cum = df[(df['year'] <= 2015) & (df['year'] >= 2005)][['blockgroup_FIPS', 
                                                                 'year',
                                                                 'cum_num_of_installations']]
# prev_year_cum.index = prev_year_cum['blockgroup_FIPS']
prev_year_cum.rename(columns={'cum_num_of_installations': 'cum_num_of_installations_prev'}, inplace=True)
prev_year_cum['year'] = prev_year_cum['year'] + 1

df_sub = df[(df['year'] <= 2016) & (df['year'] >= 2006)]
df_sub = pd.merge(df_sub, prev_year_cum, how='left', on=['blockgroup_FIPS', 'year'])

adoption_matrix_bg = pd.DataFrame(df_sub['num_of_installations'].to_numpy().reshape([-1, 11]))
adoption_matrix_bg.index = df_sub[df_sub['year'] == 2016]['blockgroup_FIPS']
adoption_matrix_bg.columns = [str(x) for x in range(2006, 2017)]

cum_adoption_matrix_prev_bg = pd.DataFrame(df_sub['cum_num_of_installations_prev'].to_numpy().reshape([-1, 11]))
cum_adoption_matrix_prev_bg.index = df_sub[df_sub['year'] == 2016]['blockgroup_FIPS']
cum_adoption_matrix_prev_bg.columns = [str(x) for x in range(2006, 2017)]

df_buildings = df[df['year'] == 2016][['blockgroup_FIPS', 'num_of_buildings_lt600']]
df_buildings.index = df_buildings['blockgroup_FIPS']

del df

# Normal Bass Model: Block group level curve fitting
adoption_params_dict = {}
i = 0
for bfips in tqdm(cum_adoption_matrix_prev_bg.index):
    i += 1
    y_arr = cum_adoption_matrix_prev_bg.loc[bfips, :].to_numpy()
    dy_arr = adoption_matrix_bg.loc[bfips, :].to_numpy()
    if np.any(dy_arr > 0):
        base = df_buildings.loc[bfips, 'num_of_buildings_lt600']
        base = max(base, 1.0)
        y_true = np.array(list(y_arr[1:]) + [y_arr[-1] + dy_arr[-1]])
        rmse, (p, q, m, d), y_pred, dy_pred, best_onset_idx = get_best_fit_NLS(y_arr, dy_arr,
                                                                               initial_m=min(base, y_true[-1]),
                                                                               upper_m=base) # BM
        r2 = r2_score(y_true, y_pred)
    else:
        p, q, m, d, rmse, r2 = 0, 0, y_arr[0], None, 0., 1.
    adoption_params_dict[bfips] = [p, q, m, d, rmse, r2]
    if i % 5000 == 0:
        adoption_params_bg = pd.DataFrame(adoption_params_dict).transpose()
        adoption_params_bg.columns = ['p', 'q', 'm', 'd', 'rmse', 'r2']
        adoption_params_bg.to_csv('results/bass_model/adoption_bass_model_params_bg.csv')
        
adoption_params_bg = pd.DataFrame(adoption_params_dict).transpose()
adoption_params_bg.columns = ['p', 'q', 'm', 'd', 'rmse', 'r2']
adoption_params_bg.to_csv('results/bass_model/adoption_bass_model_params_bg.csv')
]


#   ├── bass_model_parameter_phase_analysis.ipynb # Análise das fases dos parâmetros do modelo Bass.

│   ├── bass_model_parameter_regression.ipynb   # Análise de regressão dos parâmetros do modelo Bass.
│   ├── generate_anchor_image_dict.ipynb      # Geração de dicionário de imagens âncora.
│   ├── hp_search_HR.py              # Busca de hiperparâmetros para o modelo HR.
│   ├── hp_search_LR_rgb.py            # Busca de hiperparâmetros para o modelo LR.
│   ├── hp_search_ood_multilabels.py        # Busca de hiperparâmetros para o modelo OOD.
│   ├── __init__.py                # Inicialização do pacote.
│   ├── LICENSE.md                 # Arquivo de licença do projeto.
│   ├── LR_models/                  # Pasta contendo modelos LR.
│   │   ├── __init__.py              # Inicialização do pacote.
│   │   ├── siamese_model_binary_layerwise.py   # Modelo siamês binário com camadas.
│   │   ├── siamese_model_binary.py        # Modelo siamês binário.
│   │   └── siamese_model_rgb.py          # Modelo siamês RGB.
│   ├── predict_HR.py               # Predição utilizando o modelo HR.
│   ├── predict_installation_year_from_image_sequences.ipynb # Previsão do ano de instalação a partir de sequências de imagens.
│   ├── predict_LR_rgb.py             # Predição utilizando o modelo LR.
│   ├── predict_ood_multilabels.py         # Predição utilizando o modelo OOD.
│   ├── README.md                 # README do projeto.
│   ├── requirements.txt              # Dependências do projeto.
│   ├── solar_potential_detection.py        # Detecção de potencial solar em imagens e vídeos.
│   ├── rooftop_detection.py            # Detecção de telhados em imagens e vídeos.
│   └── utils/                     # Pasta contendo utilitários.
│       ├── image_dataset.py            # Manipulação de conjuntos de dados de imagens.
│       ├── inception_modified.py         # Modificações na arquitetura Inception.
│       └── solar_potential_calculator.py     # Cálculo do potencial solar total e valor em tempo real.








